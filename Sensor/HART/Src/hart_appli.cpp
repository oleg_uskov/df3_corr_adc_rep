#include "hart_appli.h"
#include "hart_frame.h"
//#include "corrector.h"
#include "TemperatureSensor.h"
#include "PackageTransciever.h"
#include "hart_driver.h"

extern long_addr_type long_received_addr;
extern TemperatureSensor sensor;
extern PackageTransciever HART;

unsigned int HrtByteCnt = 0;
unsigned char HrtResposeCode = 0;
unsigned char HrtDeviceStatus = 0;
unsigned char HrtResposePos = 0;
void (*command_pf)(unsigned char *data);

void set_respose_code(unsigned char *data)
{
	set_data_link( );
	data[HrtByteCnt++] = HrtResposeCode;
	data[HrtByteCnt++] = HrtDeviceStatus;
}

void set_ID(unsigned char *data)
{
	float * pDemferTime = &sensor.DemferTime;
	data[HrtByteCnt++] = 0xFE;
	data[HrtByteCnt++] = long_received_addr.ADC_id;
	data[HrtByteCnt++] = long_received_addr.unique_device_id[0];
	data[HrtByteCnt++] = HART.get_response_preamble_num() | 0xF0;
	data[HrtByteCnt++] = *((uint8_t *)pDemferTime+3);
	data[HrtByteCnt++] = *((uint8_t *)pDemferTime+2);
	data[HrtByteCnt++] = 0x70;
	data[HrtByteCnt++] = HART.get_polling_addr(long_received_addr.ADC_id);
	data[HrtByteCnt++] = HART.get_transfer_func(long_received_addr.ADC_id);
	data[HrtByteCnt++] = long_received_addr.unique_device_id[1];
	data[HrtByteCnt++] = long_received_addr.unique_device_id[2];
	data[HrtByteCnt++] = long_received_addr.unique_device_id[3];

}

static void config_change(void)
{
	HrtDeviceStatus |= 0x40;
	HART.set_config_change_flag(0xC0);
}

static void float_to_data(unsigned char *data,float *tmp)
{
	data[HrtByteCnt++] = *((unsigned char *)tmp+3);
	data[HrtByteCnt++] = *((unsigned char *)tmp+2);
	data[HrtByteCnt++] = *((unsigned char *)tmp+1);
	data[HrtByteCnt++] = *((unsigned char *)tmp);
}

static float data_to_float(unsigned char *tmp)
{
	union {
		float tmp_f;
		unsigned char buf[4];
	}U;
	unsigned char i;
	
	for(i = 0;i < 4;i++)
	{
		U.buf[i] = *((unsigned char *)tmp+3-i);
	}
	return U.tmp_f;
}

/* the same as C11 */
void C0_RdUniqueId(unsigned char *data) 
{
	set_respose_code(data);
	if(!HrtResposeCode)
	{
		set_ID(data);
	}
}

void C1_RdPV(unsigned char *data)
{
	float tmp;	
	
	set_respose_code(data);
	if(!HrtResposeCode)
	{
		tmp = HART.get_pv(long_received_addr.ADC_id);
		data[HrtByteCnt++] = HART.get_pv_unit(long_received_addr.ADC_id);
		float_to_data(data,&tmp);
	}
}

//void C2_RdLoopCurrPerOfRange(unsigned char *data)
//{
//	float tmp1,tmp2;
//
//	set_respose_code(data);
//	if(!HrtResposeCode)
//	{
//		tmp1 = get_loop_current();
//		tmp2 = get_percent_of_range();
//		float_to_data(data,&tmp1);
//		float_to_data(data,&tmp2);
//	}
//}
//
//void C3_RdDVLoopCurr(unsigned char *data)
//{
//	float tmp1,tmp2,tmp3,tmp4,tmp5;
//
//	set_respose_code(data);
//	if(!HrtResposeCode)
//	{
//		tmp1 = get_loop_current();
//		tmp2 = get_pv();
//		tmp3 = get_sv();
//		tmp4 = get_tv();
//		tmp5 = get_qv();
//		float_to_data(data,&tmp1);
//		data[HrtByteCnt++] = get_pv_unit();
//		float_to_data(data,&tmp2);
//		data[HrtByteCnt++] = get_sv_unit();
//		float_to_data(data,&tmp3);
//		data[HrtByteCnt++] = get_tv_unit();
//		float_to_data(data,&tmp4);
//		data[HrtByteCnt++] = get_qv_unit();
//		float_to_data(data,&tmp5);
//	}
//}
//
void C6_WrPollingAddr(unsigned char *data)
{
	unsigned char *dat;

	set_respose_code(data);
	if(!HrtResposeCode)
	{
		dat = get_rx_data_pointer();
		data[HrtByteCnt++] = *dat;
		HART.set_polling_addr(*dat, long_received_addr.ADC_id);

	}

}


void C34_WrPVDamping(unsigned char *data) //0x22
{
	unsigned char *dat;
	float tmp;
	
	set_respose_code(data);
	if(!HrtResposeCode)
	{
		dat = get_rx_data_pointer();
		tmp = data_to_float(dat);
		float_to_data(data,&tmp);
		HART.set_pv_damping_time(tmp);
		config_change();
		sensor.WriteConfigToFlash();

	}
}

//void C35_WrPVRange(unsigned char *data) //0x23
//{
//	unsigned char *dat;
//	float tmp1,tmp2;
//
//	set_respose_code(data);
//	if(!HrtResposeCode)
//	{
//		dat = get_rx_data_pointer();
//		tmp1 = data_to_float(dat+1);
//		tmp2 = data_to_float(dat+5);
//		data[HrtByteCnt++] = *dat;
//		float_to_data(data,&tmp1);
//		float_to_data(data,&tmp2);
//		set_ul_range_unit(*dat);
//		set_pv_upper_range(tmp1);
//		set_pv_lower_range(tmp2);
//		config_change();
//	}
//
//}

//void C36_SetPVUpperRange(unsigned char *data)
//{
//	float tmp;
//
//	set_respose_code(data);
//	if(!HrtResposeCode)
//	{
//		tmp = get_pv();
//		set_pv_upper_range(tmp);
//		config_change();
//	}
//}
//
//void C37_SetPVLowerRange(unsigned char *data)
//{
//	float tmp;
//
//	set_respose_code(data);
//	if(!HrtResposeCode)
//	{
//		tmp = get_pv();
//		set_pv_lower_range(tmp);
//		config_change();
//	}
//}
//

void C47_WrPVTransferFunction(unsigned char *data) //0x2F
{
	unsigned char *dat;

	set_respose_code(data);
	if(!HrtResposeCode)
	{
		dat = get_rx_data_pointer();
		data[HrtByteCnt++] = *dat;
		HART.set_transfer_func(*dat, long_received_addr.ADC_id);
		config_change();
		sensor.WriteConfigToFlash();
	}
}
//
//void C48_RdAdditionalDeviceStatus(unsigned char *data)
//{
//	unsigned char i;
//	unsigned char *dat;
//
//	if(!HrtResposeCode)
//	{
//		dat = get_device_specific_status();
//		for(i = 0;i < 6;i++)
//		{
//				data[HrtByteCnt++] = *(dat+i);
//		}
//		data[HrtByteCnt++] = get_extended_device_status();
//		data[HrtByteCnt++] = get_device_operating_mode();
//		data[HrtByteCnt++] = get_std_status_0();
//	}
//}
//
//void C49_WrPVTransducerSerialNum(unsigned char *data)
//{
//	unsigned char *dat;
//
//	set_respose_code(data);
//	if(!HrtResposeCode)
//	{
//		dat = get_rx_data_pointer();
//		data[HrtByteCnt++] = *dat;
//		data[HrtByteCnt++] = *(dat+1);
//		data[HrtByteCnt++] = *(dat+2);
//		set_transducer_serial_num(dat);
//		config_change();
//	}
//}
//
//void C50_RdDVAssignments(unsigned char *data)
//{
//	set_respose_code(data);
//	if(!HrtResposeCode)
//	{
//		data[HrtByteCnt++] = get_pv_code();
//		data[HrtByteCnt++] = get_sv_code();
//		data[HrtByteCnt++] = get_tv_code();
//		data[HrtByteCnt++] = get_qv_code();
//	}
//}
//


void C59_WrNumOfResposePreambles(unsigned char *data) //0x3B
{
	unsigned char *dat;
	
	set_respose_code(data);
	if(!HrtResposeCode)
	{
		dat = get_rx_data_pointer();
		data[HrtByteCnt++] = *dat;
		HART.set_response_preamble_num((*dat) & 0x0F);
		config_change();
		sensor.WriteConfigToFlash();
	}
}

//void C108_WrBurstModeCmdNum(unsigned char *data) //command_pf 1,2,3,9 should be supported by all devices
//{
//	unsigned char *dat;
//
//	set_respose_code(data);
//	if(!HrtResposeCode)
//	{
//		dat = get_rx_data_pointer();
//		data[HrtByteCnt++] = *dat;
//		set_burst_mode_cmd_num(*dat);
//		config_change();
//	}
//}
//
//void C109_BurstModeControl(unsigned char *data)
//{
//	unsigned char *dat;
//
//	set_respose_code(data);
//	if(!HrtResposeCode)
//	{
//		dat = get_rx_data_pointer();
//		data[HrtByteCnt++] = *dat;
//		set_burst_mode_code(*dat);
//	}
//}

void C129_RdPVLowerPoint(unsigned char *data) //0x81
{
	//float tmp1,tmp2;
	struct Calibrate_point temp;

	set_respose_code(data);
	if(!HrtResposeCode)
	{
		temp = HART.get_pv_lower_point(long_received_addr.ADC_id);
		float_to_data(data,&temp.U);
		float_to_data(data,&temp.R);
	}
}

void C130_RdPVUpperPoint(unsigned char *data) //0x82
{
	//float tmp1,tmp2;
	struct Calibrate_point temp;

	set_respose_code(data);
	if(!HrtResposeCode)
	{
		temp = HART.get_pv_upper_point(long_received_addr.ADC_id);
		float_to_data(data,&temp.U);
		float_to_data(data,&temp.R);
	}
}

void C131_RdVoltage(unsigned char *data) //0x83
{
	float tmp;

	set_respose_code(data);
	if(!HrtResposeCode)
	{
		tmp = HART.get_voltage(long_received_addr.ADC_id);
		data[HrtByteCnt++] = HART.get_voltage_unit();
		float_to_data(data,&tmp);
	}
}

void C132_RdfromFlash(unsigned char *data) //0x84
{
	set_respose_code(data);
	if(!HrtResposeCode)
	{
		sensor.ReadConfigFromFlash();
	}
}

void C133_SetPVLowerPoint(unsigned char *data) //0x85
{
		unsigned char *dat;
		float tmp1,tmp2;

		set_respose_code(data);
		if(!HrtResposeCode)
		{
			dat = get_rx_data_pointer();
			tmp1 = data_to_float(dat);
			tmp2 = data_to_float(dat+4);
			HART.set_pv_lower_point(tmp1, tmp2, long_received_addr.ADC_id);
			config_change();
			sensor.WriteConfigToFlash();
		}
}

void C134_SetPVUpperPoint(unsigned char *data) //0x86
{
	unsigned char *dat;
	float tmp1,tmp2;

	set_respose_code(data);
	if(!HrtResposeCode)
	{
		dat = get_rx_data_pointer();
		tmp1 = data_to_float(dat);
		tmp2 = data_to_float(dat+4);
		HART.set_pv_upper_point(tmp1, tmp2, long_received_addr.ADC_id);
		config_change();
		sensor.WriteConfigToFlash();
	}
}

void C137_RdAdditionalStatus(unsigned char *data) //0x89
{
	set_respose_code(data);
	float temp1 = HART.get_voltage(long_received_addr.ADC_id);
	float temp2 = HART.get_resistance(long_received_addr.ADC_id);
	float temp3 = HART.get_pv(long_received_addr.ADC_id);

	if(!HrtResposeCode)
	{
		float_to_data(data, & temp1);
		float_to_data(data, & temp2);
		float_to_data(data, & temp3);
		float_to_data(data,&sensor.EnvirTemperature);
	}
}

void C145_RdReferenceVoltageAndCode(unsigned char *data) //0x91
{
	float tmp1 = HART.get_referenceVoltage((enum NameOfADC)long_received_addr.ADC_id);
	float tmp2 = HART.get_reference_ADC((enum NameOfADC)long_received_addr.ADC_id);
	set_respose_code(data);
	if(!HrtResposeCode)
	{
		float_to_data(data,&tmp1);
		float_to_data(data,&tmp2);
	}
}

void C146_RdReferenceVoltage(unsigned char *data) //0x92
{
	float tmp1 = HART.get_referenceVoltage((enum NameOfADC)long_received_addr.ADC_id);
	set_respose_code(data);
	if(!HrtResposeCode)
	{
		float_to_data(data,&tmp1);
	}
}


unsigned int cmd_function(unsigned char cmd,unsigned char *data)
{

	//xmt_msg_type = get_xmt_msg_type();
//	switch(get_xmt_msg_type())
//	{
//		case XMT_BACK:
//		case XMT_ACK:
//			HrtResposeCode = 0x00;
//			HrtDeviceStatus = 0x00;
//			break;
//		case XMT_COMM_ERR:
//			HrtResposeCode = 0x88;
//			break;
//		default:
//			break;
//	}
	//unsigned char burst_cmd,is_burst_mode;
	
	//is_burst_mode = get_burst_mode_code();
	HrtByteCnt = 0;
	
//	if(is_burst_mode)
//	{
//		burst_cmd = get_burst_mode_cmd_num();
//
//		if(cmd == 109) //burst mode control : enter or exit burst mode.
//		{
//			command_pf = C109_BurstModeControl;
//		}
//		switch(burst_cmd)
//		{
//			case 1:
//				*(data-2) = 1;
//				command_pf = C1_RdPV;
//				break;
//			case 2:
//				*(data-2) = 2;
//				command_pf = C2_RdLoopCurrPerOfRange;
//				break;
//			case 3:
//				*(data-2) = 3;
//				command_pf = C3_RdDVLoopCurr;
//				break;
//			default:
//				*(data-2) = 1;
//				command_pf = C1_RdPV;
//				break;
//		}
//	}
//	else
	{
		switch(cmd)
		{
			case 0: command_pf = C0_RdUniqueId;		break;
			case 1:	command_pf = C1_RdPV;	  break;
			case 2:			break; //command_pf = C2_RdLoopCurrPerOfRange;
			case 3:			break; //command_pf = C3_RdDVLoopCurr;
			case 4:			break;
			case 5:			break;
			case 6:			break; //command_pf = C6_WrPollingAddr;
			case 7:			break; //command_pf = C7_RdLoopConfiguration;
			case 8:			break; //command_pf = C8_RdDVClass;
			case 9:			break;
			case 10:		break;
			case 11:     break;    //command_pf = C0_RdUniqueId;
			case 12:     break;    //command_pf = C12_RdMessage;
			case 13: 	 break;    //command_pf = C13_RdTagDescriptorDate;
			case 14:     break;    //command_pf = C14_RdPVTransducerInfo;
			case 15:     break;    //command_pf = C15_RdDeviceInfo;
			case 16: 	 break;    //command_pf = C16_RdFinalAssemblyNum;
			case 17:     break;    //command_pf = C17_WrMessage;
			case 18: 	 break;    //command_pf = C18_WrTagDescriptorDate;
			case 19: 	 break;    //command_pf = C19_WrFinalAssemblyNum;
			case 20:		break;
			case 21:		break;
			case 22:		break;
			case 23:        break;
			case 24:		break;
			case 25:		break;
			case 26:		break;
			case 27:		break;
			case 28:		break;
			case 29:    break;
			case 30:    break;
			case 31:    break;
			case 32:    break;
			case 33:    break;  //command_pf = C33_RdDeviceVariable;
			case 34: command_pf = C34_WrPVDamping;		break;
			case 35: 		break; //command_pf = C35_WrPVRange;
			case 36:    break;    //command_pf = C36_SetPVUpperRange;
			case 37:    break;    //command_pf = C37_SetPVLowerRange;
			case 38:    break;     //command_pf = C38_ResetCfgChangeFlag;
			case 40:    break;     //command_pf = C40_EnterOrExitFixedCurrent;
			case 41:    break;    //command_pf = C41_PerformSelfTest;
			case 42:    break;     //command_pf = C42_PerformDeviceReset;
			case 43:    break;    //command_pf = C43_PVZero;
			case 44:    break;    //command_pf = C44_WrPVUnit;
			case 45:    break;     //command_pf = C45_TrimLoopCurrentZero;
			case 46:    break;     //command_pf = C46_TrimLoopCurrentGain;
			case 47: command_pf = C47_WrPVTransferFunction;   break;
			case 48:    break;     //command_pf = C48_RdAdditionalDeviceStatus;
			case 49:    break;     //command_pf = C49_WrPVTransducerSerialNum;
			case 50:    break;     //command_pf = C50_RdDVAssignments;
			case 51:    break;     //command_pf = C51_WrDVAssignments;
			case 59: command_pf = C59_WrNumOfResposePreambles;		break;
			case 108: 	break;   //command_pf = C108_WrBurstModeCmdNum;
			case 109: 	break;   //command_pf = C109_BurstModeControl;
			case 129: command_pf = C129_RdPVLowerPoint;  break;
			case 130: command_pf = C130_RdPVUpperPoint;  break;
			case 131: command_pf = C131_RdVoltage;  break;
			case 132: command_pf = C132_RdfromFlash;  break;
			case 133: command_pf = C133_SetPVLowerPoint;  break;
			case 134: command_pf = C134_SetPVUpperPoint;  break;
			case 137: command_pf = C137_RdAdditionalStatus;  break;
			case 145: command_pf = C145_RdReferenceVoltageAndCode;  break;
			case 146: command_pf = C146_RdReferenceVoltage;  break;

			default:
				break;
		}
	}
	enter_critical_section( );
	command_pf(data);
	exit_critical_section( );
	return HrtByteCnt;
}

void hart_appli_init(void)
{
// 	unsigned char dst_buf[6] = {
// 		0x04,0x10,0x41,0x00,0x00,0x00
// 	};
//	unsigned char buf[] = "HARTTEST";
//	unsigned char dst[6];
	//packed_ascii(buf,sizeof(buf),dst,6);
	//set_tag(dst);
	//serical init
	//serial_init(1200,8,HT_SERICAL_EVEN,1);
	SerialReceiveMsg = hart_rcv_msg;
	SerialSendMsg = hart_xmt_msg;
	serial_enable(TRUE,FALSE);
	//set_burst_mode_code(FALSE);
	HART.set_response_preamble_num(PREAMBLE_DEFAULT_NUM);
	HART.set_polling_addr(1,1);
	HART.set_polling_addr(2,2);
//	set_extended_device_status(MAINTANANCE_REQUIRED);
//	set_std_status_0(NON_VOLATILE_MEMORY_DEFECT);
	//set_loop_current(4.0f);
// 	typedef void (*PerformSelfTest)(void);
//   typedef void (*PerformDeviceReset)(void);
//   typedef void (*TrimLoopCurrent)(void *data); ///////////////??????????????
}

// void hart_appli_poll(void)
// {
// 	unsigned char xmt_msg_type;
// 	unsigned char hart_state;
// 	
// 	hart_state = get_hart_state();
// 	
// 	if(hart_state == HRT_PROCESS)
// 	{
// 		xmt_msg_type = get_xmt_msg_type();
// 		switch(xmt_msg_type)
// 		{
// 			case XMT_BACK:
// 			case XMT_ACK:
// 				HrtResposeCode = 0x00;
// 				HrtDeviceStatus = 0x00;
// 				break;
// 			case XMT_COMM_ERR:
// 				HrtResposeCode = 0x88;
// 				break;
// 			default:
// 				break;
// 		}
// 		frame_cmd_data(cmd_function);
// 		set_tx_byte_count(HrtByteCnt);
// 		hart_appli_completed_notify(TRUE);
// 	}
// 	hart_poll();	
// }







