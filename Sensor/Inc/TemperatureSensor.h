/*
 * TemperatureSensor.h
 *
 *  Created on: Jan 5, 2021
 *      Author: Oleg
 */

#ifndef SRC_TEMPERATURESENSOR_H_
#define SRC_TEMPERATURESENSOR_H_

#include <array>
#include <math.h>

struct Calibrate_point
{
    float R;
    float U;
};

// единицы измерения
enum Units
{
	celsiy = 0x20,
	pascal = 0x5C,
	mVolt = 0xFE,
};

// состояния работы с тремя АЦП
enum ADC_state
{
	IDLE,
	WAIT_ENVIR_TEMPERATURE,
	WAIT_TEMPERATURE_1,
	WAIT_TEMPERATURE_2,
	WAIT_PRESSURE,
};

// Имя АЦП
enum NameOfADC
{
	TEMPERATURE_1 = 1,
	TEMPERATURE_2 = 2,
	PRESSURE = 3,
};

// тип термо элемента
enum TermoTypeElement
{
	PLATINUM_3_911 = 1,
	PLATINUM_3_851 = 2,
	COPPER_4_28 = 3,
	COPPER_4_26 = 4,
};


// Класс для вычисление температуры газа на овновании значиний от АЦП
class TemperatureSensor {
public:
	TemperatureSensor();
	virtual ~TemperatureSensor();

	static constexpr float Ro = 100.0; /*сопротивление при 0 градусов по Цельсию*/

	inline static constexpr std::array<float,4> D_3_851_array = {255.819, 9.14550, -2.92363, 1.79090}; /*массив коэффициентов D для α = 3,851e-3*/
	static constexpr double A_3_851 = 3.9083e-3; /* платина*/
	static constexpr double B_3_851 = -5.775e-7; /* платина*/

	inline static constexpr std::array<float,4> D_3_911_array = {251.903, 8.80035, -2.91506, 1.67611}; /*массив коэффициентов D для α = 3,911e-3*/
	static constexpr double A_3_911 = 3.9690e-3; /*платина*/
	static constexpr double B_3_911 = -5.841e-7; /*платина*/

	inline static constexpr std::array<float,4> D_4_28_array = {233.87, 7.9370,  -2.0062,  -0.3953}; /*массив коэффициентов D для α = 3,851e-3*/
	static constexpr double A_4_28 = 4.28e-3; /*коэффициентов D для α = 4,28e-3 (медь) */
	static constexpr double A_4_26 = 4.26e-3; /*коэффициентов D для α = 4,26e-3 (медь) */

	float Vref_T1 = 0.400; /*измеренное реальное напряжение в вольтах */
	float Vref_T2 = 0.400; /*измеренное реальное напряжение в вольтах */
	uint16_t EnvirTemperature_ADC;
	float ADC1_evg_data;
	float ADC2_evg_data;
	float Voltage_ADC1;
	float Voltage_ADC2;
	float Voltage_ADC3;
	float Reference_ADC1;
	float Reference_ADC2;
	float Resist_ADC1;
	float Resist_ADC2;
	float Resist_ADC3;
	float EnvirTemperature; /* температура окружающей среды */
	float Temperature_1channel = 0.0;
	float Temperature_2channel = 0.0;
	float Pressure_channel = 0.0;
	float DemferTime = 2; /* текущее время демфирования */
	float Udemf ; /* среднее напряжение за время демфирования */
//	unsigned char flash_data_buf[50]; /* буфер данных для записи во flash */
//	unsigned char FlashByteCnt; /* текущее количество байт в буфере данных для записи во flash */

	Calibrate_point point_min_T1 = {82.79, 0.083}; // -40 градусов, медь
	Calibrate_point point_max_T1 = {142.80, 0.143}; // 100 градусов, медь
	Calibrate_point point_min_T2 = {82.79, 0.083}; // -40 градусов, медь
	Calibrate_point point_max_T2 = {142.80, 0.143}; // 100 градусов, медь

	void Run(void);
	void Init(void);
	float CalculateRealTemperatureRun(void);
	float GetCurrentResistance(float, enum NameOfADC nameOfADC); /* */
	float GetTemperature(float U_average, enum NameOfADC nameOfADC); /* */
	float ReadReferenceVoltage(enum NameOfADC nameOfADC); /*получение опорного напряжения*/
	void WriteTemperToFlash(void);
	void WriteConfigToFlash(void);
	void ReadConfigFromFlash(void);
	TermoTypeElement currentTermoTypeElement_T1 = PLATINUM_3_851;
	TermoTypeElement currentTermoTypeElement_T2 = PLATINUM_3_851;
	ADC_state currentADC_state = IDLE;

private:
	//unsigned m_number_of_conver;
	unsigned cnt1_samples;
	unsigned cnt2_samples;
	unsigned cnt3_samples;

	unsigned long m_summa_samples_1;
	unsigned long m_summa_samples_2;
	unsigned long m_summa_samples_3;

	bool m_finish_init = false;

};

#endif /* SRC_TEMPERATURESENSOR_H_ */
