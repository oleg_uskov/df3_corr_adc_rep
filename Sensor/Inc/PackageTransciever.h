/*
 * PackageTransciever.h
 *
 *  Created on: 9 янв. 2021 г.
 *      Author: Oleg
 */

#ifndef PACKAGETRANSCIEVER_H_
#define PACKAGETRANSCIEVER_H_

#include "main.h"
#include "TemperatureSensor.h"

extern TemperatureSensor sensor;

class PackageTransciever {
public:
	PackageTransciever();
	virtual ~PackageTransciever();

	void Init(void);
	void HART_run(void);
	void TimerTick(void);



	uint8_t PollingAddr [3] = {1,2,3};
	uint8_t ResposePreambleNum = 0;
	uint8_t ConfigChangeFlag = 0;
//	float PVDampingTime = 8;
//	float PVUpperRange = 0.0;
//	float PVlowerRange = 0.0;
//	float ULRangeUnit = 0.0;

	float get_pv(unsigned char ADC_id)  { return ADC_id == 1 ? sensor.Temperature_1channel :
										(ADC_id == 2 ? sensor.Temperature_2channel : sensor.Pressure_channel); }
	float get_voltage(unsigned char ADC_id)  {return ADC_id == 1 ? sensor.Voltage_ADC1 :
										(ADC_id == 2 ? sensor.Voltage_ADC2 : sensor.Voltage_ADC3);  }
	float get_resistance(unsigned char ADC_id)  {return ADC_id == 1 ? sensor.Resist_ADC1 :
										(ADC_id == 2 ? sensor.Resist_ADC2 : sensor.Resist_ADC3);  }
	float get_referenceVoltage(unsigned char ADC_id)  {return ADC_id == 1 ? sensor.Vref_T1 :
										(ADC_id == 2 ? sensor.Vref_T2 : 0.0);  }

	float get_reference_ADC(unsigned char ADC_id)  {return ADC_id == 1 ? sensor.Reference_ADC1 :
										(ADC_id == 2 ? sensor.Reference_ADC2 : 0.0);  }

	/* DV unit */
	//void set_pv_unit(unsigned char pv_unit) { para.PVUnit = pv_unit; }
	unsigned char get_pv_unit(unsigned char ADC_id) { return ADC_id == 1 ? Units::celsiy :
													(ADC_id == 2 ? Units::celsiy : Units::pascal); }
	unsigned char get_voltage_unit(void) {return Units::mVolt; }

	/* polling address and loop current mode */
	void set_polling_addr(unsigned char polling_addr, unsigned char ADC_id) {if (ADC_id < 4)  PollingAddr[ADC_id-1] = polling_addr; }

	unsigned char get_polling_addr(unsigned char ADC_id) { return PollingAddr[ADC_id-1]; }

	void set_pv_upper_point(float volt, float resist, unsigned char ADC_id) {
		if (ADC_id == 1) sensor.point_max_T1 = {resist, volt};
		else sensor.point_max_T2 = {resist, volt};
	}

	struct Calibrate_point get_pv_upper_point(unsigned char ADC_id) {
		if (ADC_id == 1) return {sensor.point_max_T1.R, sensor.point_max_T1.U};
		else return {sensor.point_max_T2.R, sensor.point_max_T2.U};
	}

	void set_pv_lower_point(float volt, float resist, unsigned char ADC_id) {
		if (ADC_id == 1) sensor.point_min_T1 = {resist, volt};
		else sensor.point_min_T2 = {resist, volt};
	}

	struct Calibrate_point get_pv_lower_point(unsigned char ADC_id) {
		if (ADC_id == 1) return {sensor.point_min_T1.R, sensor.point_min_T1.U};
		else return {sensor.point_min_T2.R, sensor.point_min_T2.U};
	}

	void set_pv_damping_time(float damping_time) { sensor.DemferTime = damping_time; }
	float get_pv_damping_time(void) { return sensor.DemferTime; }
	void set_transfer_func(unsigned char tsf_func, unsigned char ADC_id) {
		if (ADC_id == 1) sensor.currentTermoTypeElement_T1 = (enum TermoTypeElement) tsf_func;
		else sensor.currentTermoTypeElement_T2 = (enum TermoTypeElement) tsf_func;
	}

	uint8_t get_transfer_func(unsigned char ADC_id) {
		if (ADC_id == 1) return (uint8_t)sensor.currentTermoTypeElement_T1;
		else return (uint8_t)sensor.currentTermoTypeElement_T2;
	}

	/* numblers of response preambles */
	void set_response_preamble_num(unsigned char rsp_preamble_num) { ResposePreambleNum = rsp_preamble_num; }
	unsigned char get_response_preamble_num(void) { return ResposePreambleNum; }
	///* configuration change flag */
	void set_config_change_flag(unsigned char cfg_change_flag) { ConfigChangeFlag = cfg_change_flag; }
	unsigned char get_config_change_flag(void) { return ConfigChangeFlag; }


	static const int MAX_SIZE_REC_BUFFER = 10;
	uint8_t received_data_arr[MAX_SIZE_REC_BUFFER] = {0,};
private:



};

#endif /* PACKAGETRANSCIEVER_H_ */
