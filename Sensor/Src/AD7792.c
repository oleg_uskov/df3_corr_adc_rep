/***************************************************************************//**
 *   @file   AD7792.c
 *   @brief  Implementation of AD7792 Driver.
 *   @author Bancisor MIhai
********************************************************************************
 * Copyright 2012(c) Analog Devices, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *  - The use of this software may or may not infringe the patent rights
 *    of one or more patent holders.  This license does not release you
 *    from the requirement that you obtain separate licenses from these
 *    patent holders to use this software.
 *  - Use of the software either in source or binary form, must be run
 *    on or directly connected to an Analog Devices Inc. component.
 *
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
********************************************************************************
 *   SVN Revision: 501
*******************************************************************************/

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/
#include "AD7792.h"				// AD7792 definitions.

/***************************************************************************//**
 * @brief Initializes the AD7792 and checks if the device is present.
 *
 * @return status - Result of the initialization procedure.
 *                  Example: 1 - if initialization was successful (ID is 0x0B).
 *                           0 - if initialization was unsuccessful.
*******************************************************************************/
unsigned char AD7792_Init(unsigned char modifyCS)
{ 
	unsigned char status = 0x1;
    
	Set_CS_LOW(modifyCS);
    if((AD7792_GetRegisterValue(AD7792_REG_ID, 1, modifyCS) & 0x0F) != AD7792_ID)
	{
		status = 0x0;
	}
    Set_CS_HIGH(modifyCS);
    
	return(status);
}

/***************************************************************************//**
 * @brief Sends 32 consecutive 1's on SPI in order to reset the part.
 *
 * @return  None.    
*******************************************************************************/
void AD7792_Reset(unsigned char modifyCS)
{
	unsigned char dataToSend[5] = {0x00, 0xff, 0xff, 0xff, 0xff};
	
    Set_CS_LOW(modifyCS);
	SPI_Write(dataToSend,4);
    Set_CS_HIGH(modifyCS);
}
/***************************************************************************//**
 * @brief Reads the value of the selected register
 *
 * @param regAddress - The address of the register to read.
 * @param size - The size of the register to read.
 *
 * @return data - The value of the selected register register.
*******************************************************************************/
unsigned long AD7792_GetRegisterValue(unsigned char regAddress, 
                                      unsigned char size,
                                      unsigned char modifyCS)
{
	unsigned char data[5]      = {0x00, 0x00, 0x00, 0x00, 0x00};
	unsigned long receivedData = 0x00;
    unsigned char i            = 0x00; 
    
	data[0] = 0x01 * modifyCS;
	data[1] = AD7792_COMM_READ |  AD7792_COMM_ADDR(regAddress); 
	SPI_Read(data,(1 + size));
	for(i = 1;i < size + 1;i ++)
    {
        receivedData = (receivedData << 8) + data[i];
    }
    
    return (receivedData);
}
/***************************************************************************//**
 * @brief Writes the value to the register
 *
 * @param -  regAddress - The address of the register to write to.
 * @param -  regValue - The value to write to the register.
 * @param -  size - The size of the register to write.
 *
 * @return  None.    
*******************************************************************************/
void AD7792_SetRegisterValue(unsigned char regAddress,
                             unsigned long regValue, 
                             unsigned char size,
                             unsigned char modifyCS)
{
	unsigned char data[5]      = {0x00, 0x00, 0x00, 0x00, 0x00};	
	unsigned char* dataPointer = (unsigned char*)&regValue;
    unsigned char bytesNr      = size + 1;
    
    data[0] = 0x01 * modifyCS;
    data[1] = AD7792_COMM_WRITE |  AD7792_COMM_ADDR(regAddress);
    while(bytesNr > 1)
    {
        data[bytesNr] = *dataPointer;
        dataPointer ++;
        bytesNr --;
    }	    
	SPI_Write(data,(1 + size));
}
/***************************************************************************//**
 * @brief  Waits for RDY pin to go low.
 *
 * @return None.
*******************************************************************************/
void AD7792_WaitRdyGoLow(void)
{
    while( AD7792_RDY_STATE )
    {
        ;
    }
}

/***************************************************************************//**
 * @brief Sets the operating mode of AD7792.
 *
 * @param mode - Mode of operation.
 *
 * @return  None.    
*******************************************************************************/
void AD7792_SetMode(unsigned long mode, unsigned char numberCS)
{
    unsigned long command;
    
    command = AD7792_GetRegisterValue(AD7792_REG_MODE,
                                      2,
									  numberCS); // CS is modified by SPI read/write functions.
    command &= ~AD7792_MODE_SEL(0xFF);
    command |= AD7792_MODE_SEL(mode);
    command &= ~AD7792_MODE_RATE(0xFF);
    command |= AD7792_MODE_RATE(10);
    AD7792_SetRegisterValue(
            AD7792_REG_MODE,
            command,
            2, 
			numberCS); // CS is modified by SPI read/write functions.
}
/***************************************************************************//**
 * @brief Selects the channel of AD7792.
 *
 * @param  channel - ADC channel selection.
 *
 * @return  None.    
*******************************************************************************/
void AD7792_SetChannel(unsigned long channel, unsigned char numberCS)
{
    unsigned long command;
    
    command = AD7792_GetRegisterValue(AD7792_REG_CONF,
                                      2,
									  numberCS); // CS is modified by SPI read/write functions.
    command &= ~AD7792_CONF_CHAN(0xFF);
    command |= AD7792_CONF_CHAN(channel);
    AD7792_SetRegisterValue(
            AD7792_REG_CONF,
            command,
            2,
			numberCS); // CS is modified by SPI read/write functions.
}

/***************************************************************************//**
 * @brief  Sets the gain of the In-Amp.
 *
 * @param  gain - Gain.
 *
 * @return  None.    
*******************************************************************************/
void AD7792_SetGain(unsigned long gain, unsigned char numberCS)
{
    unsigned long command;
    
    command = AD7792_GetRegisterValue(AD7792_REG_CONF,
                                      2,
									  numberCS); // CS is modified by SPI read/write functions.
    command &= ~AD7792_CONF_GAIN(0xFF);
    command |= AD7792_CONF_GAIN(gain);
    AD7792_SetRegisterValue(
            AD7792_REG_CONF,
            command,
            2,
			numberCS); // CS is modified by SPI read/write functions.
}

/***************************************************************************//**
 * @brief  Sets the unipolar code.
 *
 * @param
 *
 * @return  None.
*******************************************************************************/
void AD7792_SetUNIPOLAR( unsigned char numberCS)
{
    unsigned long command;

    command = AD7792_GetRegisterValue(AD7792_REG_CONF,
                                      2,
									  numberCS); // CS is modified by SPI read/write functions.
    command |= AD7792_CONF_UNIPOLAR;
    AD7792_SetRegisterValue(
            AD7792_REG_CONF,
            command,
            2,
			numberCS); // CS is modified by SPI read/write functions.
}
/***************************************************************************//**
 * @brief Sets the reference source for the ADC.
 *
 * @param type - Type of the reference.
 *               Example: AD7792_REFSEL_EXT	- External Reference Selected
 *                        AD7792_REFSEL_INT	- Internal Reference Selected.
 *
 * @return None.    
*******************************************************************************/
void AD7792_SetIntReference(unsigned char type, unsigned char numberCS)
{
    unsigned long command = 0;
    
    command = AD7792_GetRegisterValue(AD7792_REG_CONF,
                                      2,
									  numberCS); // CS is modified by SPI read/write functions.
    command &= ~AD7792_CONF_REFSEL(AD7792_REFSEL_INT);
    command |= AD7792_CONF_REFSEL(type);
    AD7792_SetRegisterValue(AD7792_REG_CONF,
							command,
							2,
							numberCS); // CS is modified by SPI read/write functions.
}

/***************************************************************************//**
 * @brief Performs the given calibration to the specified channel.
 *
 * @param mode - Calibration type.
 * @param channel - Channel to be calibrated.
 *
 * @return none.
*******************************************************************************/
void AD7792_Calibrate(unsigned char mode, unsigned char channel, unsigned char numberCS)
{
    unsigned short oldRegValue = 0x0;
    unsigned short newRegValue = 0x0;
    
    AD7792_SetChannel(channel, numberCS);
    oldRegValue &= AD7792_GetRegisterValue(AD7792_REG_MODE, 2, numberCS); // CS is modified by SPI read/write functions.
    oldRegValue &= ~AD7792_MODE_SEL(0x7);
    newRegValue = oldRegValue | AD7792_MODE_SEL(mode);
    Set_CS_LOW(numberCS);
    AD7792_SetRegisterValue(AD7792_REG_MODE, newRegValue, 2, 0); // CS is not modified by SPI read/write functions.
    AD7792_WaitRdyGoLow();
    Set_CS_HIGH(numberCS);
    
}

/***************************************************************************//**
 * @brief Returns the result of a single conversion.
 *
 * @return regData - Result of a single analog-to-digital conversion.
*******************************************************************************/
unsigned long AD7792_SingleConversion(unsigned char numberCS)
{
    unsigned long command = 0x0;
    unsigned long regData = 0x0;
    
    command  = AD7792_MODE_SEL(AD7792_MODE_SINGLE) | AD7792_MODE_RATE(10);
    Set_CS_LOW(numberCS);
    AD7792_SetRegisterValue(AD7792_REG_MODE,
                            command,
                            2,
                            0);// CS is not modified by SPI read/write functions.
    AD7792_WaitRdyGoLow();
    regData = AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0); // CS is not modified by SPI read/write functions.
    Set_CS_HIGH(numberCS);

    return(regData);
}

/***************************************************************************//**
 * @brief Returns the average of several conversion results.
 *
 * @return samplesAverage - The average of the conversion results.
*******************************************************************************/
unsigned long AD7792_ContinuousReadAvg(unsigned char sampleNumber, unsigned char numberCS)
{
    unsigned long samplesAverage = 0x0;
    unsigned long command        = 0x0;
    unsigned char count          = 0x0;
    
    command = AD7792_MODE_SEL(AD7792_MODE_CONT) | AD7792_MODE_RATE(10);
    Set_CS_LOW(numberCS);
    AD7792_SetRegisterValue(AD7792_REG_MODE,
                            command,
                            2,
                            0);// CS is not modified by SPI read/write functions.
    for(count = 0;count < sampleNumber;count ++)
    {
        AD7792_WaitRdyGoLow();
        samplesAverage += AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0);  // CS is not modified by SPI read/write functions.
    }
    Set_CS_HIGH(numberCS);
    samplesAverage = samplesAverage / sampleNumber;
    
    return(samplesAverage);
}




/***************************************************************************//**
 * @brief Returns .
 *
 * @return s.
*******************************************************************************/
//unsigned long AD7792_NotBlockContinuousRead(unsigned char numberCS)
//{
//    Set_CS_LOW(numberCS);
//    AD7792_SetRegisterValue(AD7792_REG_MODE,
//                            command,
//                            2,
//                            0);// CS is not modified by SPI read/write functions.
//    return 0;
//}

//void SPIP_EnableSCK_EXTI(SPIP_t* pSPIP) {
//	SYSCFG->EXTICR[0] &= ~(0xF<<4);
//	SYSCFG->EXTICR[0] |= (3<<4);
//	EXTI->IMR1 |= GPIO_PIN_1; // interrupt enable
//	EXTI->RTSR1 |= GPIO_PIN_1; // rising sense
//	EXTI->FTSR1 |= GPIO_PIN_1; // falling sense
//	// Above is the ONLY WAY to have EXTI on AF pin.
//	__HAL_GPIO_EXTI_CLEAR_IT(pSPIP->pSCK->Init.Pin);
//	__HAL_GPIO_EXTI_CLEAR_FLAG(pSPIP->pSCK->Init.Pin);
//	//EXTI->IMR1 |= (1<<1);
//	HAL_NVIC_SetPriority(EXTI1_IRQn, 1, 0);
//	HAL_NVIC_EnableIRQ(EXTI1_IRQn);
//	}
//
//	void SPIP_DisableSCK_EXTI(SPIP_t* pSPIP) {
//	HAL_NVIC_DisableIRQ(EXTI1_IRQn);
//}
//
//void EXTI1_IRQHandler(void)
//{
///* USER CODE BEGIN EXTI0_IRQn 0 */
///* USER CODE END EXTI0_IRQn 0 */
//HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
///* USER CODE BEGIN EXTI0_IRQn 1 */
///* USER CODE END EXTI0_IRQn 1 */
//
//}
