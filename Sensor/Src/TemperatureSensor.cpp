/*
 * TemperatureSensor.cpp
 *
 *  Created on: Jan 5, 2021
 *      Author: Oleg
 */

#include "TemperatureSensor.h"
#include "main.h"
//#include <string>

#include "AD7792.h"				// AD7792 definitions.

extern UART_HandleTypeDef huart1;
extern SPI_HandleTypeDef hspi3;
extern addrTemper addrTemper1;

extern "C" {
	extern void P23K256_Write_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *txData, uint16_t TxSize);
	extern uint8_t P23K256_Read_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *rxData, uint16_t RxSize);

}

TemperatureSensor::TemperatureSensor()
{
	cnt1_samples = 0;
	cnt2_samples = 0;
	cnt3_samples = 0;
	m_summa_samples_1 = 0;
	m_summa_samples_2 = 0;
	m_summa_samples_3 = 0;
}



TemperatureSensor::~TemperatureSensor() {

}

/********************************************************************************
 * Инициализация всех АЦП
 *********************************************************************************/
void TemperatureSensor::Init() {
	ReadConfigFromFlash(); // восстановление кофнигурационных файлов из Flash

	AD7792_Reset(TEMPERATURE_1);
	AD7792_Reset(TEMPERATURE_2);
	if (!AD7792_Init(TEMPERATURE_1))
		Error_Handler();
	if (!AD7792_Init(TEMPERATURE_2))
		Error_Handler();

/*настройка первого АЦП*/
    AD7792_SetRegisterValue(AD7792_REG_IO,
    		AD7792_IEXCEN(AD7792_EN_IXCEN_1mA), //устанвка тока 1 мА на оба источника
			1, TEMPERATURE_1);

    AD7792_SetGain(AD7792_GAIN_2, TEMPERATURE_1);                // set gain to 2
    AD7792_SetChannel(AD7792_CH_AIN1P_AIN1M, TEMPERATURE_1);     // use AIN1(+) - AIN1(-)
    AD7792_SetMode(AD7792_MODE_CONT, TEMPERATURE_1); // режим непрерывного преобразования

    Vref_T1 = ReadReferenceVoltage(TEMPERATURE_1);
    //AD7792_SetUNIPOLAR(TEMPERATURE_1);

    //AD7792_SetIntReference(AD7792_REFSEL_INT);    // select internal 1.17V reference
//    AD7792_Calibrate(AD7792_MODE_CAL_INT_ZERO,
//                     AD7792_CH_AIN1P_AIN1M);      // Internal Zero-Scale Calibration

//	AD7792_SetChannel(AD7792_CH_TEMP);

    //AD7792_SetMode(AD7792_MODE_SINGLE, TEMPERATURE_1); // режим одиночного преобразования

/*настройка второго АЦП*/
    AD7792_SetRegisterValue(AD7792_REG_IO,
    		AD7792_IEXCEN(AD7792_EN_IXCEN_1mA), //устанвка тока 1 мА на оба источника
			1, TEMPERATURE_2);

    AD7792_SetGain(AD7792_GAIN_2, TEMPERATURE_2);                // set gain to 2
    AD7792_SetChannel(AD7792_CH_AIN1P_AIN1M, TEMPERATURE_2);     // use AIN1(+) - AIN1(-)
	AD7792_SetMode(AD7792_MODE_CONT, TEMPERATURE_2); // режим непрерывного преобразования
	Vref_T2 = ReadReferenceVoltage(TEMPERATURE_2);

	m_finish_init = true;


}

/********************************************************************************
 * Автомат последовательного считывания данных с АЦП
 *********************************************************************************/
void TemperatureSensor::Run() {

//	AD7792_SetChannel(AD7792_CH_AIN1P_AIN1M, TEMPERATURE_1);
//	m_summa_ADC1 += AD7792_SingleConversion(TEMPERATURE_1);
//	m_number_of_conver++;
//	AD7792_SetChannel(AD7792_CH_TEMP, TEMPERATURE_1);
//	EnvirTemperature = ((AD7792_SingleConversion(TEMPERATURE_1)- 32768) * 117000 /(32768 * 81)) - 276;
//
	int ready = ADC_DATA_READY;
	switch(currentADC_state){
	case IDLE:
		if (m_finish_init) {
			//Set_CS_LOW(TEMPERATURE_1);
			if(cnt1_samples == 0) { //читаем только раз за цикл
				AD7792_SetChannel(AD7792_CH_TEMP, TEMPERATURE_1);
				currentADC_state = WAIT_ENVIR_TEMPERATURE;
			}
			else
				currentADC_state = WAIT_TEMPERATURE_1;
		}
		break;
	case WAIT_ENVIR_TEMPERATURE:
			Set_CS_LOW(TEMPERATURE_1);
			if(ready){
				EnvirTemperature_ADC = (AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0)); // 0xFFFF -  потому что перепут в схеме подключения резистора
				EnvirTemperature = (1170.0 * (EnvirTemperature_ADC - 32768)/(32768 * 0.81)) - 276; // перевод с градусы Цельсия
				AD7792_SetChannel(AD7792_CH_AIN1P_AIN1M, TEMPERATURE_1);
				Set_CS_HIGH(TEMPERATURE_1);
				currentADC_state = WAIT_TEMPERATURE_1;
			}
		break;
	case WAIT_TEMPERATURE_1:
		Set_CS_LOW(TEMPERATURE_1);
		if(ready){
			m_summa_samples_1 += (0xFFFF - AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0)); // 0xFFFF -  потому что перепут в схеме подключения резистора
			Set_CS_HIGH(TEMPERATURE_1);
			cnt1_samples++;
			currentADC_state = WAIT_TEMPERATURE_2;
		}
		break;
	case WAIT_TEMPERATURE_2:
		Set_CS_LOW(TEMPERATURE_2);
		if(ADC_DATA_READY){
			m_summa_samples_2 += (0xFFFF - AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0)); // 0xFFFF -  потому что перепут в схеме подключения резистора
			Set_CS_HIGH(TEMPERATURE_2);
			cnt2_samples++;
			currentADC_state = IDLE; // WAIT_PRESSURE;
		}
		break;
//	case WAIT_PRESSURE:
//		Set_CS_LOW(PRESSURE);
//		if(ADC_DATA_READY){
//			m_summa_samples_3 += AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0);
//			Set_CS_HIGH(PRESSURE);
//			cnt3_samples++;
//			currentADC_state = IDLE;
//		}
//		break;
	default:
		//ConsolePrint("Unknown state", 14);
		break;
	}
}

/********************************************************************************
 * Получение среднего значения от АЦП и расчет текущей температуры
 *********************************************************************************/
float TemperatureSensor::CalculateRealTemperatureRun() {

	//int32_t result_average =0; //AD7792_ContinuousReadAvg(1);
	/* вычисляем среднее значение */
	if (cnt1_samples != 0) ADC1_evg_data = m_summa_samples_1/cnt1_samples;

	if (cnt2_samples != 0) ADC2_evg_data = m_summa_samples_2/cnt2_samples;

	m_summa_samples_1 = 0;
	m_summa_samples_2 = 0;
	m_summa_samples_3 = 0;
	cnt1_samples = 0;
	cnt2_samples = 0;
	cnt3_samples = 0;
	/* вычисляем текущее напряжение на резисторе */
	Voltage_ADC1 = Vref_T1 *  (ADC1_evg_data - 32768)/32768/2; // делим на 2 тк коэф. усиления 2
	Voltage_ADC2 = Vref_T2 *  (ADC2_evg_data - 32768)/32768/2; // делим на 2 тк коэф. усиления 2
	Temperature_1channel = GetTemperature(Voltage_ADC1, TEMPERATURE_1);
	Temperature_2channel = GetTemperature(Voltage_ADC2, TEMPERATURE_2);

	WriteTemperToFlash();
//	HAL_Delay(1);
//	ReadFromFlash();
	return Temperature_1channel;
	//return AD7792_GetRegisterValue( AD7792_REG_DATA,2,1);

}



/********************************************************************************
 * Расчет текущей температуры
 *********************************************************************************/
float TemperatureSensor::GetTemperature(float U_average, enum NameOfADC nameOfADC)
{
	TermoTypeElement type = (nameOfADC == TEMPERATURE_1) ? currentTermoTypeElement_T1 : currentTermoTypeElement_T2;

	float currResist = GetCurrentResistance(U_average, nameOfADC);
	float Temperature = 0.0;
	if (type == PLATINUM_3_851){
		if (currResist < Ro){ // температура меньше 0
			for (char i=1; i < D_3_851_array.size()  + 1; i++) {
				Temperature += D_3_851_array[i-1] * pow( (currResist/Ro - 1), i );
			}
		}else{
			Temperature = (sqrt(pow(A_3_851,2) - 4 * B_3_851 * (1 - currResist/Ro)) - A_3_851) / (2 * B_3_851);

		}
	} else if (type == PLATINUM_3_911){
		if (currResist < Ro){ // температура меньше 0
			for (char i=1; i < D_3_911_array.size()  + 1; i++) {
				Temperature += D_3_911_array[i-1] * pow( (currResist/Ro - 1), i );
			}
		}else{
			Temperature = (sqrt(pow(A_3_911,2) - 4 * B_3_911 * (1 - currResist/Ro)) - A_3_911) / (2 * B_3_911);

		}
	} else if (type == COPPER_4_28){
		if (currResist < Ro){
			for (char i=1; i < D_4_28_array.size() + 1; i++) {
				Temperature += D_4_28_array[i-1] *  pow( (currResist/Ro - 1) , i);
			}
		}else{
			Temperature = (currResist/Ro - 1)/A_4_28;
		}
	} else if (type == COPPER_4_26){
		Temperature = (currResist/Ro - 1)/A_4_26;
	}else{
		Temperature = 0.0;
		//Error_Handler();
	}

	//HAL_UART_Transmit(&huart1, (uint8_t*)info.c_str(), info.length(), 1000);
	return Temperature;
}


/********************************************************************************
 * Расчет текущего сопротивления
 *********************************************************************************/
float TemperatureSensor::GetCurrentResistance(float U_av, enum NameOfADC nameOfADC)
{
	float SummaADC = 0;
	uint8_t N = 2 * (uint8_t)DemferTime; /* количество отсчетов за время демфировани (*2 тк 2 раза в секунду считываем) */
	static int numb_new_data1 = 1; /* количество заполненных отсчетов в массиве*/
	static int numb_new_data2 = 1; /* количество заполненных отсчетов в массиве*/
	static std::array<float,20> shift_reg_temper1 = {0,}; /*буфер для хранения данных для подсчета скользящего среднего */
	static std::array<float,20> shift_reg_temper2 = {0,}; /*буфер для хранения данных для подсчета скользящего среднего */

	if (N > shift_reg_temper1.size())
		return 0.0;

	if (nameOfADC == TEMPERATURE_1){
		for (char i=0; i < N-1; i++) {
			shift_reg_temper1[i] =shift_reg_temper1[i+1];
			SummaADC += shift_reg_temper1[i];
		}
		shift_reg_temper1[N-1] = U_av;
		SummaADC += shift_reg_temper1[N-1];
		if (numb_new_data1 < N){
		   Udemf = SummaADC / numb_new_data1;
		   numb_new_data1++;
		}
		else
		   Udemf = SummaADC / N ;
	}
	else if (nameOfADC == TEMPERATURE_2){
		for (char i=0; i < N-1; i++) {
			shift_reg_temper2[i] =shift_reg_temper2[i+1];
			SummaADC += shift_reg_temper2[i];
		}
		shift_reg_temper2[N-1] = U_av;
		SummaADC += shift_reg_temper2[N-1];
		if (numb_new_data2 < N){
		   Udemf = SummaADC / numb_new_data2;
		   numb_new_data2++;
		}
		else
		   Udemf = SummaADC / N ;
	}


	float currResist ; // текущее значпние сопротивления

	if (nameOfADC == TEMPERATURE_1){
		Resist_ADC1 = currResist = ((Udemf - point_min_T1.U) * (point_max_T1.R - point_min_T1.R))/ (point_max_T1.U - point_min_T1.U) + point_min_T1.R; // формула линейной интерполяции;
	}
	else if (nameOfADC == TEMPERATURE_2){
		Resist_ADC2 = currResist = ((Udemf - point_min_T2.U) * (point_max_T2.R - point_min_T2.R))/ (point_max_T2.U - point_min_T2.U) + point_min_T2.R;

	}
	return currResist;
}

//void float_to_data(unsigned char *data,float *tmp)
//{
//	data[FlashByteCnt++] = *((unsigned char *)tmp+3);
//	data[FlashByteCnt++] = *((unsigned char *)tmp+2);
//	data[FlashByteCnt++] = *((unsigned char *)tmp+1);
//	data[FlashByteCnt++] = *((unsigned char *)tmp);
//}
//
//float data_to_float(unsigned char *tmp)
//{
//	union {
//		float tmp_f;
//		unsigned char buf[4];
//	}U;
//	unsigned char i;
//
//	for(i = 0;i < 4;i++)
//	{
//		U.buf[i] = *((unsigned char *)tmp+3-i);
//	}
//	return U.tmp_f;
//}

/********************************************************************************
 * Запись текущей температуры во flash
 *********************************************************************************/
void TemperatureSensor::WriteTemperToFlash()
{
//	float_to_data(flash_data_buf,&Temperature_1channel);
//	float_to_data(flash_data_buf,&EnvirTemperature);
	float data[] = {Temperature_1channel,
					ADC1_evg_data,
					Temperature_2channel,
					ADC2_evg_data,
					EnvirTemperature,
					(float)EnvirTemperature_ADC,
	};
	uint8_t * p_data = reinterpret_cast<uint8_t *>(&data);
	P23K256_Write_Data(&hspi3, addrTemper1.addrDataTemper,	p_data, sizeof(data));

}

/********************************************************************************
 * Запись во flash текущего состояния
 *********************************************************************************/
void TemperatureSensor::WriteConfigToFlash()
{
//	float_to_data(flash_data_buf,&Temperature_1channel);
//	float_to_data(flash_data_buf,&EnvirTemperature);
	float data[] = {point_min_T1.R,
					point_min_T1.U,
					point_max_T1.R,
					point_max_T1.U,
					point_min_T2.R,
					point_min_T2.U,
					point_max_T2.R,
					point_max_T2.U,
					DemferTime,
					(float)currentTermoTypeElement_T1,
					(float)currentTermoTypeElement_T2,
	};
	uint8_t * p_data = reinterpret_cast<uint8_t *>(&data);
	P23K256_Write_Data(&hspi3, addrTemper1.addrConfigTemper,	p_data, sizeof(data));

}

/********************************************************************************
 * Чтение из flash текущего состояния
 *********************************************************************************/
void TemperatureSensor::ReadConfigFromFlash()
{
	float data[16];
    P23K256_Read_Data(&hspi3, addrTemper1.addrConfigTemper, (uint8_t*)&data,sizeof(data));
	point_min_T1.R                 = data[0];
	point_min_T1.U                 = data[1];
	point_max_T1.R                 = data[2];
	point_max_T1.U                 = data[3];
	point_min_T2.R                 = data[4];
	point_min_T2.U                 = data[5];
	point_max_T2.R                 = data[6];
	point_max_T2.U                 = data[7];
	DemferTime                     = data[8];
	currentTermoTypeElement_T1     = (TermoTypeElement)data[9];
	currentTermoTypeElement_T2     = (TermoTypeElement)data[10];

}


/********************************************************************************
 * Чтение опорного напряжения
 *********************************************************************************/
float TemperatureSensor::ReadReferenceVoltage(enum NameOfADC nameOfADC) {
	m_finish_init = false; //запрет основного цикла работа
	constexpr uint8_t NUMBER_SAMPLE = 4;
	while(currentADC_state != IDLE) {};
    AD7792_SetIntReference(AD7792_REFSEL_INT, nameOfADC);    // select internal 1.17V reference
	AD7792_SetChannel(AD7792_CH_AIN3P_AIN3M, nameOfADC);
	float refVolt = 0;
	float Reference_code = 0;
	Set_CS_LOW(nameOfADC);
    for(int count = 0; count < NUMBER_SAMPLE; count ++)
    {
        AD7792_WaitRdyGoLow();
    	Reference_code += (AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0));
    }
    Reference_code = Reference_code / NUMBER_SAMPLE;

	if (nameOfADC == TEMPERATURE_1){
		Reference_ADC1 = Reference_code;
	}
	else if (nameOfADC == TEMPERATURE_2){
		Reference_ADC2 = Reference_code; ;

	}
	refVolt = 1.17 * (Reference_code - 32768) / 32768 /2; // перевод в вольт
	AD7792_SetChannel(AD7792_CH_AIN1P_AIN1M, nameOfADC);
    AD7792_SetIntReference(AD7792_REFSEL_EXT, nameOfADC);    // select external
	Set_CS_HIGH(nameOfADC);

	m_finish_init = true; //разрешение основного цикла работа
	return refVolt;

}
