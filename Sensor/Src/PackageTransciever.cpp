/*
 * PackageTransciever.cpp
 *
 *  Created on: 9 янв. 2021 г.
 *      Author: Oleg
 */

#include "PackageTransciever.h"
#include "string.h"
#include "hart_appli.h"
#include "soft_timer.h"
#include "hart_driver.h"
#include "hart_frame.h"


extern UART_HandleTypeDef huart1;

PackageTransciever::PackageTransciever() {

}

/********************************************************************************
 * Инициализация HART
 *********************************************************************************/
void PackageTransciever::Init() {

	soft_timer_init();
	hart_appli_init();

	/*включаем прием по прерыванию */
	//HAL_UART_Receive_IT(&huart1, (uint8_t*)received_data_arr, 1);
	/*также разрешаем прерывание если тишина на линии после приема нескольких байт */
	//__HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);

}

PackageTransciever::~PackageTransciever() {
	// TODO Auto-generated destructor stub
}

/********************************************************************************
 * Основной цикл HART
 *********************************************************************************/
void  PackageTransciever::HART_run()
{
	hart_poll();
}

/********************************************************************************
 * Запуск счетчика HART
 *********************************************************************************/
void  PackageTransciever::TimerTick()
{
	TimerTickHandler();
}

//void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
//{
//        if(huart == &huart1)
//        {
//                uint32_t er = HAL_UART_GetError(&huart1);
//                HAL_GPIO_WritePin(RTS_U1_GPIO_Port, RTS_U1_Pin, GPIO_PIN_SET);/* включение передачи по UART1*/
//                switch(er)
//                {
//
//                        case HAL_UART_ERROR_PE:
//                                HAL_UART_Transmit(&huart1, (uint8_t*)"ERR_Callbck - Parity error\n", 27, 1000);
//                                __HAL_UART_CLEAR_PEFLAG(&huart1);
//                                huart->ErrorCode = HAL_UART_ERROR_NONE;
//                        break;
//
//                        case HAL_UART_ERROR_NE:
//                                HAL_UART_Transmit(&huart1, (uint8_t*)"ERR_Callbck - Noise error\n", 26, 1000);
//                                __HAL_UART_CLEAR_NEFLAG(&huart1);
//                                huart->ErrorCode = HAL_UART_ERROR_NONE;
//                        break;
//
//                        case HAL_UART_ERROR_FE:
//                                HAL_UART_Transmit(&huart1, (uint8_t*)"ERR_Callbck - Frame error\n", 26, 1000);
//                                __HAL_UART_CLEAR_FEFLAG(&huart1);
//                                huart->ErrorCode = HAL_UART_ERROR_NONE;
//                        break;
//
//                        case HAL_UART_ERROR_ORE:
//                                HAL_UART_Transmit(&huart1, (uint8_t*)"ERR_Callbck - Overrun error\n", 28, 1000);
//                                __HAL_UART_CLEAR_OREFLAG(huart);
//                                huart->ErrorCode = HAL_UART_ERROR_NONE;
//                        break;
//
//                        case HAL_UART_ERROR_DMA:
//                                HAL_UART_Transmit(&huart1, (uint8_t*)"ERR_Callbck - DMA transfer error\n", 33, 1000);
//                                huart->ErrorCode = HAL_UART_ERROR_NONE;
//                        break;
//
//                        default:
//                        break;
//                }
//        }
//}
