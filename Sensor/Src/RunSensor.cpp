/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.cpp
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <math.h>

#include "LiquidCrystal.h"

#include "stm32l4xx_hal_rtc.h"
#include "TemperatureSensor.h"
#include "PackageTransciever.h"
#include "hart_driver.h"
//#include "AD7792.h"				// AD7792 definitions.
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef  struct bintime {
   unsigned char seconds;
   unsigned char minutes;
   unsigned char hours;
   unsigned char date;
   unsigned char month;
   unsigned char year;
} Time;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
extern RTC_HandleTypeDef hrtc;

extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
extern SPI_HandleTypeDef hspi3;

extern TIM_HandleTypeDef htim16;
extern TIM_HandleTypeDef htim17;

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

extern DMA_HandleTypeDef hdma_usart2_rx;
extern DMA_HandleTypeDef hdma_usart2_tx;

extern  uint32_t t_17, t_16;
/* USER CODE BEGIN PV */
uint32_t t_17,_secTakt,counttime, t_16;
Time     TIME_SETTIME, BINTIME; 
  RTC_TimeTypeDef sTime;  //= {0};
  RTC_DateTypeDef sDate; //= {0};
  RTC_AlarmTypeDef sAlarm;// = {0};
uint32_t alt1, alt2, alt3, count1, count2, count3;
uint32_t ctim1, ctim2, ctim3;
uint8_t  bflag_c1,bflag_c2;
uint32_t intr1, intr2, intr3;
uint32_t Interval[3]= {0,0,0};  //интервад между импульсами
float Qw_s1, Qw_s3, Qw_s2;  // секундный расход счетчиков газа
uint16_t P = 0;
uint32_t k,in1,in2,k1;
//char strd[40];
char str90[90];

TemperatureSensor sensor;
PackageTransciever HART;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/

/* USER CODE BEGIN PFP */

void LiquidCrystal(GPIO_TypeDef *gpioport,uint16_t d0, uint16_t d1,
    uint16_t d2, uint16_t d3,uint16_t d4, uint16_t d5, uint16_t d6, uint16_t d7);
void LCDPrint(uint8_t stroka, uint8_t colonka, char * Text);
void setCursor(uint8_t col, uint8_t row);
size_t print(const char *);


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
//===========================================================================
// Вывод на экран LCD
void LCDPrint(uint8_t stroka, uint8_t colonka, char * Text)
{
  setCursor(colonka, stroka);
  print(Text);
 }

// Вывод на экран LCD
void ConsolePrint(char * Text, int len)
{
	HAL_GPIO_WritePin(U2_RTS_GPIO_Port, U2_RTS_Pin, GPIO_PIN_SET);/* включение передачи по UART1*/
	HAL_UART_Transmit(&huart2, (uint8_t*)Text, len, 10000);
	HAL_GPIO_WritePin(U2_RTS_GPIO_Port, U2_RTS_Pin, GPIO_PIN_RESET);/* выключение передачи по UART1*/

}


//===========================================================================
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{// прерывания по таймерам TIM17 - через 5 мс, TIM16 - через 1 мс
//struct tq   *ukT;
//int t;
    
   if (*(&htim->Instance)  == TIM17)
   {

		if (t_17==0)  //(t_17==200)
		{_secTakt++;
	//	 t_17=0;
		 if (_secTakt == 60)
			 {_secTakt = 0; } //counttime = 0; }
		}
		t_17++;
		counttime++; // = t_17;
   }
   else if (*(&htim->Instance)  == TIM16)
   {
		//HART.TimerTick();
		t_16++;
		if ((t_16 % 500) == 0) {// каждые полсекунды
			//sensor.CalculateRealTemperatureRun();
		}
		if ((t_16 % 5) == 0) {// каждые 5мс
			//sensor.Run(); /*периодически запускаем считывание данных со всех АЦП */
		}

   }
}
////----------------------------------------------------
//void UART_EndTxTransfer(UART_HandleTypeDef *huart)
//{
//#if defined(USART_CR1_FIFOEN)
//  /* Disable TXEIE, TCIE, TXFT interrupts */
//  CLEAR_BIT(huart->Instance->CR1, (USART_CR1_TXEIE_TXFNFIE | USART_CR1_TCIE));
//  CLEAR_BIT(huart->Instance->CR3, (USART_CR3_TXFTIE));
//#else
//  /* Disable TXEIE and TCIE interrupts */
//  CLEAR_BIT(huart->Instance->CR1, (USART_CR1_TXEIE | USART_CR1_TCIE));
//#endif /* USART_CR1_FIFOEN */
//
//  /* At end of Tx process, restore huart->gState to Ready */
//  huart->gState = HAL_UART_STATE_READY;
//}

//-----------------------------------------------------------
void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{// прерывание от чвсов RTC через 1 сек
	struct bintime    *pbintime;
	uint32_t val1, val2, val3, ct1, ct2, ct3;
    
    pbintime = &TIME_SETTIME;
    ct1 = count1; ct2 = count2; 
    counttime = 0;
    t_17 = 0;
    HAL_RTC_GetTime(hrtc, &sTime, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(hrtc, &sDate, RTC_FORMAT_BIN);
    BINTIME.seconds = sTime.Seconds; 
    BINTIME.minutes = sTime.Minutes;
    BINTIME.hours = sTime.Hours;
    BINTIME.date = sDate.Date;
    BINTIME.month = sDate.Month;
    BINTIME.year = sDate.Year;
 // подсчет импульсов от счетчиков газа
// для первого счетчика Fl-1      
   in1 = t_16 - k;
   k = t_16;
      if (alt1 > ct1) {          // было пеpеполнение счетчика
	    val1 = 0xFFFFFFFF - alt1;
	    val1 += ct1;
	    val1++;
      }
      else                             // нет пеpеполнения счетчика
        val1 = ct1-alt1;
      alt1 = ct1;
      if (Interval[0] < 96)
          Qw_s1    = (float)val1; // секундный расход

//Вівод на LCD дата-время 1 раз в сек
//      memset(str90,0,90);
//      sprintf(str90," %02u.%02u.%02u  %02u:%02u:%02u ",
//      BINTIME.date,BINTIME.month,BINTIME.year%100,
//      BINTIME.hours,BINTIME.minutes,BINTIME.seconds);
//      LCDPrint(3,0,str90);  // вывод на дисплей LCD в нижнюю строку

      memset(str90,0,90);
      sprintf(str90, "U1 = %f_", sensor.Voltage_ADC1);
      LCDPrint(0,0,str90);  // вывод на дисплей LCD в 1 строку

      memset(str90,0,90);
      //sprintf(str90, "Toc = %d", (int)sensor.EnvirTemperature);
      sprintf(str90, "T1 = %f_", sensor.Temperature_1channel);
      LCDPrint(1,0,str90);  // вывод на дисплей LCD в 2 строку

      memset(str90,0,90);
      sprintf(str90, "U2 = %f_", sensor.Voltage_ADC2);
      LCDPrint(2,0,str90);  // вывод на дисплей LCD в 3 строку

      memset(str90,0,90);
      //sprintf(str90, "Toc = %d", (int)sensor.EnvirTemperature);
      sprintf(str90, "T2 = %f_", sensor.Temperature_2channel);
      LCDPrint(3,0,str90);  // вывод на дисплей LCD в 4 строку

//      memset(str90,0,90);
//      sprintf(str90, "ADC2 = %d", (int)sensor.ADC2_evg_data);
//      LCDPrint(1,0,str90);  // вывод на дисплей LCD в 1 строку


}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */

void InitSensor(void)
{
	t_17 = 0;
	t_16 = 0;
	LiquidCrystal(GPIOD, D0_Pin, D1_Pin, D2_Pin, D3_Pin, D4_Pin, D5_Pin, D6_Pin,D7_Pin);
	_secTakt = 0;
	count1 = 0;
	count2 = 0;
	intr1 = 0;
	intr2 = 0;;
	alt1 = 0;
	alt2 = 0;
	bflag_c1  = 0;
	bflag_c2  = 0;
	ctim1 = 0;
	ctim2 = 0;
	k1=0; k=0;
	HAL_GPIO_WritePin(GPIOD, LED_Pin, GPIO_PIN_SET); // подстветка дисплея ВКЛ


	HAL_TIM_Base_Start_IT(&htim17);
	HAL_TIM_Base_Start_IT(&htim16);
	HAL_GPIO_WritePin(GPIOA, ADC1_Pin, GPIO_PIN_SET);//отключение АЦП1
	HAL_GPIO_WritePin(GPIOA, ADC2_Pin, GPIO_PIN_SET);//отключение АЦП2
	HAL_GPIO_WritePin(GPIOA, ADC3_Pin, GPIO_PIN_SET);//отключение АЦП3
	sensor.Init();
	HART.Init();

}

void RunSensor(void)
{
	HART.HART_run();
	if ((t_16 % 500) == 0) {// каждые полсекунды
		sensor.CalculateRealTemperatureRun();
	}
	if ((t_16 % 5) == 0) {// каждые 5мс
		sensor.Run(); /*периодически запускаем считывание данных со всех АЦП */
		//HART.TimerTick();
	}
	if ((t_16 % 2) == 0) {// каждые 2мс
		HART.TimerTick();
	}
}

/********************************************************************************
* запуск HART
 *********************************************************************************/
void  HART_start()
{
	serial_enable(true,false);
}

/********************************************************************************
* остановка HART
 *********************************************************************************/
void HART_stop()
{
	serial_enable(false,false);
}

/********************************************************************************
* возвращает значение температуры по первому каналу
 *********************************************************************************/
float Temperature1()
{
	return sensor.Temperature_1channel;
}

/********************************************************************************
* возвращает значение температуры по второму каналу
 *********************************************************************************/
float Temperature2()
{
	return sensor.Temperature_2channel;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
