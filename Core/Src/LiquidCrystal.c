#include "stm32l4xx_hal.h" // change this line accordingly

#include <stdio.h>
#include <string.h>
#include <main.h>
#include "LiquidCrystal.h"

uint8_t _fourbit_mode = 0;
uint8_t dotsize = LCD_5x10DOTS;//LCD_5x8DOTS;

uint16_t _rs_pin;

uint16_t _enable_pin;
uint16_t _data_pins[8];

GPIO_TypeDef *_port;

uint8_t _numlines;

uint8_t _row_offsets[4];
/**/

const unsigned char RusChar[] = {
		0x41,       //код 0xC0, символ 'А'
		0xA0,       //код 0xC1, символ 'Б'
		0x42,       //код 0xC2, символ 'В'
		0xA1,       //код 0xC3, символ 'Г'
		0xE0,       //код 0xC4, символ 'Д'
		0x45,       //код 0xC5, символ 'Е'
		0xA3,       //код 0xC6, символ 'Ж'
		0xA4,       //код 0xC7, символ 'З'
		0xA5,       //код 0xC8, символ 'И'
		0xA6,       //код 0xC9, символ 'Й'
		0x4B,       //код 0xCA, символ 'К'
		0xA7,       //код 0xCB, символ 'Л'
		0x4D,       //код 0xCC, символ 'М'
		0x48,       //код 0xCD, символ 'Н'
		0x4F,       //код 0xCE, символ 'О'
		0xA8,       //код 0xCF, символ 'П'
		0x50,       //код 0xD0, символ 'Р'
		0x43,       //код 0xD1, символ 'С'
		0x54,       //код 0xD2, символ 'Т'
		0xA9,       //код 0xD3, символ 'У'
		0xAA,       //код 0xD4, символ 'Ф'
		0x58,       //код 0xD5, символ 'Х'
		0xE1,       //код 0xD6, символ 'Ц'
		0xAB,       //код 0xD7, символ 'Ч'
		0xAC,       //код 0xD8, символ 'Ш'
		0xE2,       //код 0xD9, символ 'Щ'
		0xAD,       //код 0xDA, символ 'Ъ'
		0xAE,       //код 0xDB, символ 'Ы'
		0x62,       //код 0xDC, символ 'Ь'
		0xAF,       //код 0xDD, символ 'Э'
		0xB0,       //код 0xDE, символ 'Ю'
		0xB1,       //код 0xDF, символ 'Я'
		0x61,       //код 0xE0, символ 'а'
		0xB2,       //код 0xE1, символ 'б'
		0xB3,       //код 0xE2, символ 'в'
		0xB4,       //код 0xE3, символ 'г'
		0xE3,       //код 0xE4, символ 'д'
		0x65,       //код 0xE5, символ 'е'
		0xB6,       //код 0xE6, символ 'ж'
		0xB7,       //код 0xE7, символ 'з'
		0xB8,       //код 0xE8, символ 'и'
		0xB9,       //код 0xE9, символ 'й'
		0xBA,       //код 0xEA, символ 'к'
		0xBB,       //код 0xEB, символ 'л'
		0xBC,       //код 0xEC, символ 'м'
		0xBD,       //код 0xED, символ 'н'
		0x6F,       //код 0xEE, символ 'о'
		0xBE,       //код 0xEF, символ 'п'
		0x70,       //код 0xF0, символ 'р'
		0x63,       //код 0xF1, символ 'с'
		0xBF,       //код 0xF2, символ 'т'
		0x79,       //код 0xF3, символ 'у'
		0xE4,       //код 0xF4, символ 'ф'
		0x78,       //код 0xF5, символ 'х'
		0xE5,       //код 0xF6, символ 'ц'
		0xC0,       //код 0xF7, символ 'ч'
		0xC1,       //код 0xF8, символ 'ш'
		0xE6,       //код 0xF9, символ 'щ'
		0xC2,       //код 0xFA, символ 'ъ'
		0xC3,       //код 0xFB, символ 'ы'
		0xC4,       //код 0xFC, символ 'ь'
		0xC5,       //код 0xFD, символ 'э'
		0xC6,       //код 0xFE, символ 'ю'
		0xC7        //код 0xFF, символ 'я'
		};



void LiquidCrystal(GPIO_TypeDef *gpioport,
			     uint16_t d0, uint16_t d1, uint16_t d2, uint16_t d3,
				 uint16_t d4, uint16_t d5, uint16_t d6, uint16_t d7)
{
	DWT_Initial();
    init(0, gpioport, d0, d1, d2, d3, d4, d5, d6, d7);

}

void init(uint8_t fourbitmode, GPIO_TypeDef *gpioport,
			 uint16_t d0, uint16_t d1, uint16_t d2, uint16_t d3,
			 uint16_t d4, uint16_t d5, uint16_t d6, uint16_t d7)
{
  _rs_pin = RS_LCD_Pin;
  _enable_pin = E_LCD_Pin;
  _port = gpioport;
  
  _data_pins[0] = d0;
  _data_pins[1] = d1;
  _data_pins[2] = d2;
  _data_pins[3] = d3; 
  _data_pins[4] = d4;
  _data_pins[5] = d5;
  _data_pins[6] = d6;
  _data_pins[7] = d7;

  delay_micros(50000);

  begin(20, 4);
}

void begin(uint8_t cols, uint8_t lines) {

	_numlines = lines;

  //setRowOffsets(0x00, 0x40, 0x00 + cols, 0x40 + cols);
  setRowOffsets(0x00, 0x40, 0x14, 0x54);

	HAL_GPIO_WritePin(GPIOC, _rs_pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, _enable_pin, GPIO_PIN_SET);

	command(0x30);

	delay_micros(1000);
	command(0x38);
	delay_micros(1000);
	command(0x38);
	HAL_Delay(1);

	command(0x0F);
	delay_micros(1000);
	command(0x01);
	delay_micros(2000);
	command(0x06);
	delay_micros(1000);

	command(0x0C);
	delay_micros(1000);

	command(0x02);
	delay_micros(2000);

	// display();
	//  clear();

	// Initialize to default text direction (for romance languages)
	  _displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
	  // set the entry mode
	  command(LCD_ENTRYMODESET | _displaymode);

  }
void setDRAMModel(uint8_t model) {
  _dram_model = model;
}

void setRowOffsets(int row0, int row1, int row2, int row3)
{
  _row_offsets[0] = row0;
  _row_offsets[1] = row1;
  _row_offsets[2] = row2;
  _row_offsets[3] = row3;
}




/********** high level commands, for the user! */
void clear(void)
{
  command(LCD_CLEARDISPLAY);  // clear display, set cursor position to zero
  //delay_micros(2000);  // this command takes a long time!
  HAL_Delay(2);
}

void home(void)
{
  command(LCD_RETURNHOME);
  delay_micros(2000);  // this command takes a long time!
}

void setCursor(uint8_t col, uint8_t row)
{
  const size_t max_lines = sizeof(_row_offsets) / sizeof(*_row_offsets);
  if ( row >= max_lines ) {
    row = max_lines - 1;    // we count rows starting w/0
  }
  if ( row >= _numlines ) {
    row = _numlines - 1;    // we count rows starting w/0
  }
  command(LCD_SETDDRAMADDR | (col + _row_offsets[row]));
}

// Turn the display on/off (quickly)
void noDisplay() {
	_displaycontrol &= ~LCD_DISPLAYON;
	  command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void display() {

	  _displaycontrol |= LCD_DISPLAYON;
	  command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void noCursor() {
	 _displaycontrol &= ~LCD_CURSORON;
	  command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void cursor() {
	_displaycontrol |= LCD_CURSORON;
	  command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turn on and off the blinking cursor
void noBlink() {
	 _displaycontrol &= ~LCD_BLINKON;
	  command(LCD_DISPLAYCONTROL | _displaycontrol);

}
void blink() {
	_displaycontrol |= LCD_BLINKON;
	  command(LCD_DISPLAYCONTROL | _displaycontrol);

}

// These commands scroll the display without changing the RAM
void scrollDisplayLeft() {
	command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}
void scrollDisplayRight() {
	command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

// This is for text that flows Left to Right
void leftToRight() {

	_displaymode |= LCD_ENTRYLEFT;
	  command(LCD_ENTRYMODESET | _displaymode);
}

// This is for text that flows Right to Left
void rightToLeft() {

	 _displaymode &= ~LCD_ENTRYLEFT;
	  command(LCD_ENTRYMODESET | _displaymode);
}

// This will 'right justify' text from the cursor
void autoscroll() {
	 _displaymode |= LCD_ENTRYSHIFTINCREMENT;
	  command(LCD_ENTRYMODESET | _displaymode);
}

// This will 'left justify' text from the cursor
void noAutoscroll() {
	_displaymode &= ~LCD_ENTRYSHIFTINCREMENT;
	  command(LCD_ENTRYMODESET | _displaymode);
}




// This will print character string to the LCD
size_t print(const char str[]) {
	if (str == NULL)
		return 0;


	const uint8_t *buffer = (const uint8_t*) str;

	size_t size = strlen(str);
	size_t n = 0;
	
	/*while (size--) {
		if (_write(*buffer++))
			n++;
		else
			break;
	}*/
	while (size--) {
			if (_write(*buffer++))
				n++;
			else
				break;
		}
	return n;
}





int iL;
// Allows us to fill the first 8 CGRAM locations
// with custom characters
void createChar(uint8_t location, uint8_t charmap[]) {
  location &= 0x7; // we only have 8 locations 0-7
  command(LCD_SETCGRAMADDR | (location << 3));
  for (iL=0; iL<8; iL++) {
    _write(charmap[iL]);
  }
}


void LCD_build(uint8_t location, uint8_t *ptr){
    uint8_t j;
      if(location<8){
          command(0x40+(location*8));
          delay_micros(40);
          for(j=0;j<8;j++)
             _write(ptr[j]);
     }

}





/*********** mid level commands, for sending data/cmds */

inline void command(uint8_t value) {
  //send(value, GPIO_PIN_RESET);
	send(value, GPIO_PIN_SET);
}

inline size_t _write(uint8_t value) {
	if(value>=192){
		send(RusChar[value-192], GPIO_PIN_RESET);
	}else {
		send(value, GPIO_PIN_RESET);
        }
  return 1; // assume sucess
}



/************ low level data pushing commands **********/

// write either command or data, with automatic 4/8-bit selection
void send(uint8_t value, GPIO_PinState mode) {
  HAL_GPIO_WritePin(GPIOC, _rs_pin, mode);


  write_bits(value);

}

void pulseEnable(void) {
  HAL_GPIO_WritePin(GPIOA, _enable_pin, GPIO_PIN_RESET);
  delay_micros(1);
  HAL_GPIO_WritePin(GPIOA, _enable_pin, GPIO_PIN_SET);
  delay_micros(450);    // enable pulse must be >450ns
  HAL_GPIO_WritePin(GPIOA, _enable_pin, GPIO_PIN_RESET);
  delay_micros(40);   // commands need > 37us to settle
}


void write_bits(uint8_t value) {

	for (iL = 0; iL < 8; iL++) {

		HAL_GPIO_WritePin(_port, _data_pins[iL],
				((value >> iL) & 0x01) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	}
	pulseEnable();
}

void DWT_Initial()
{
	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk; // разрешаем использовать счётчик
	//DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;   // запускаем счётчик
}

void delay_micros(uint32_t us)
{
    uint32_t us_count_tic =  us * (SystemCoreClock / 1000000); // получаем кол-во тактов за 1 мкс и умножаем на наше значение
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;   // запускаем счётчик
    DWT->CYCCNT = 0U; // обнуляем счётчик
    while(DWT->CYCCNT < us_count_tic);
}

